import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
var translate = require('node-google-translate-skidz')

export default class TranslatesController {
    public async translate({ request, response, session }: HttpContextContract) {
        const payload = request.body()

        let d = await translate({
            text: payload.original,
            source: 'auto',
            target: payload.target
        }, function (result) {
            let data = result
            return data
        })
        
        session.put('changed', d.translation)
        response.redirect().toPath('/')
    }
}
